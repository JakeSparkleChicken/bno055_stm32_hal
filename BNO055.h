/*
 * BNO055.h
 *
 *  Created on: Jan 17, 2019
 *      Author: JakeSparkleChicken
 *      License: Public Domain
 */

#include "stm32f7xx_hal.h"
#include <math.h>

#ifndef BNO055_H_
#define BNO055_H_

#define BNO055_I2C_ADDRESS						((uint8_t)0xA0>>1)		//Address is 7-bit so it needs to be shifted

/* Page 0 Registers */
#define BNO055_MAG_RADIUS_MSB					((uint8_t)0x6A)
#define BNO055_MAG_RADIUS_LSB					((uint8_t)0x69)
#define BNO055_ACC_RADIUS_MSB					((uint8_t)0x68)
#define BNO055_ACC_RADIUS_LSB					((uint8_t)0x67)
#define BNO055_GYR_OFFSET_Z_MSB					((uint8_t)0x66)
#define BNO055_GYR_OFFSET_Z_LSB					((uint8_t)0x65)
#define BNO055_GYR_OFFSET_Y_MSB					((uint8_t)0x64)
#define BNO055_GYR_OFFSET_Y_LSB					((uint8_t)0x63)
#define BNO055_GYR_OFFSET_X_MSB					((uint8_t)0x62)
#define BNO055_GYR_OFFSET_X_LSB					((uint8_t)0x61)
#define BNO055_MAG_OFFSET_Z_MSB					((uint8_t)0x60)
#define BNO055_MAG_OFFSET_Z_LSB					((uint8_t)0x5F)
#define BNO055_MAG_OFFSET_Y_MSB					((uint8_t)0x5E)
#define BNO055_MAG_OFFSET_Y_LSB					((uint8_t)0x5D)
#define BNO055_MAG_OFFSET_X_MSB					((uint8_t)0x5C)
#define BNO055_MAG_OFFSET_X_LSB					((uint8_t)0x5B)
#define BNO055_ACC_OFFSET_Z_MSB					((uint8_t)0x5A)
#define BNO055_ACC_OFFSET_Z_LSB					((uint8_t)0x59)
#define BNO055_ACC_OFFSET_Y_MSB					((uint8_t)0x58)
#define BNO055_ACC_OFFSET_Y_LSB					((uint8_t)0x57)
#define BNO055_ACC_OFFSET_X_MSB					((uint8_t)0x56)
#define BNO055_ACC_OFFSET_X_LSB					((uint8_t)0x55)
#define BNO055_AXIS_MAP_SIGN					((uint8_t)0x42)
#define BNO055_AXIS_MAP_CONFIG					((uint8_t)0x41)
#define BNO055_TEMP_SOURCE						((uint8_t)0x40)
#define BNO055_SYS_TRIGGER						((uint8_t)0x3F)
#define BNO055_PWR_MODE							((uint8_t)0x3E)
#define BNO055_OPR_MODE							((uint8_t)0x3D)
#define BNO055_UNIT_SEL							((uint8_t)0x3B)
#define BNO055_SYS_ERROR						((uint8_t)0x3A)
#define BNO055_SYS_STATUS						((uint8_t)0x39)
#define BNO055_SYS_CLK_STATUS					((uint8_t)0x38)
#define BNO055_INT_STA							((uint8_t)0x37)
#define BNO055_ST_RESULT						((uint8_t)0x36)
#define BNO055_CALIB_STAT						((uint8_t)0x35)
#define BNO055_TEMP								((uint8_t)0x34)
#define BNO055_GRV_DATA_Z_MSB					((uint8_t)0x33)
#define BNO055_GRV_DATA_Z_LSB					((uint8_t)0x32)
#define BNO055_GRV_DATA_Y_MSB					((uint8_t)0x31)
#define BNO055_GRV_DATA_Y_LSB					((uint8_t)0x30)
#define BNO055_GRV_DATA_X_MSB					((uint8_t)0x2F)
#define BNO055_GRV_DATA_X_LSB					((uint8_t)0x2E)
#define BNO055_LIA_DATA_Z_MSB					((uint8_t)0x2D)
#define BNO055_LIA_DATA_Z_LSB					((uint8_t)0x2C)
#define BNO055_LIA_DATA_Y_MSB					((uint8_t)0x2B)
#define BNO055_LIA_DATA_Y_LSB					((uint8_t)0x2A)
#define BNO055_LIA_DATA_X_MSB					((uint8_t)0x29)
#define BNO055_LIA_DATA_X_LSB					((uint8_t)0x28)
#define BNO055_QUA_DATA_Z_MSB					((uint8_t)0x27)
#define BNO055_QUA_DATA_Z_LSB					((uint8_t)0x26)
#define BNO055_QUA_DATA_Y_MSB					((uint8_t)0x25)
#define BNO055_QUA_DATA_Y_LSB					((uint8_t)0x24)
#define BNO055_QUA_DATA_X_MSB					((uint8_t)0x23)
#define BNO055_QUA_DATA_X_LSB					((uint8_t)0x22)
#define BNO055_QUA_DATA_W_MSB					((uint8_t)0x21)
#define BNO055_QUA_DATA_W_LSB					((uint8_t)0x20)
#define BNO055_EULER_PITCH_MSB					((uint8_t)0x1F)
#define BNO055_EULER_PITCH_LSB					((uint8_t)0x1E)
#define BNO055_EULER_ROLL_MSB					((uint8_t)0x1D)
#define BNO055_EULER_ROLL_LSB					((uint8_t)0x1C)
#define BNO055_EULER_HEADING_MSB				((uint8_t)0x1B)
#define BNO055_EULER_HEADING_LSB				((uint8_t)0x1A)
#define BNO055_GYR_DATA_Z_MSB					((uint8_t)0x19)
#define BNO055_GYR_DATA_Z_LSB					((uint8_t)0x18)
#define BNO055_GYR_DATA_Y_MSB					((uint8_t)0x17)
#define BNO055_GYR_DATA_Y_LSB					((uint8_t)0x16)
#define BNO055_GYR_DATA_X_MSB					((uint8_t)0x15)
#define BNO055_GYR_DATA_X_LSB					((uint8_t)0x14)
#define BNO055_MAG_DATA_Z_MSB					((uint8_t)0x13)
#define BNO055_MAG_DATA_Z_LSB					((uint8_t)0x12)
#define BNO055_MAG_DATA_Y_MSB					((uint8_t)0x11)
#define BNO055_MAG_DATA_Y_LSB					((uint8_t)0x10)
#define BNO055_MAG_DATA_X_MSB					((uint8_t)0x0F)
#define BNO055_MAG_DATA_X_LSB					((uint8_t)0x0E)
#define BNO055_ACC_DATA_Z_MSB					((uint8_t)0x0D)
#define BNO055_ACC_DATA_Z_LSB					((uint8_t)0x0C)
#define BNO055_ACC_DATA_Y_MSB					((uint8_t)0x0B)
#define BNO055_ACC_DATA_Y_LSB					((uint8_t)0x0A)
#define BNO055_ACC_DATA_X_MSB					((uint8_t)0x09)
#define BNO055_ACC_DATA_X_LSB					((uint8_t)0x08)
#define BNO055_PAGE_ID							((uint8_t)0x07)
#define BNO055_BL_REV_ID						((uint8_t)0x06)
#define BNO055_SW_REV_ID_MSB					((uint8_t)0x05)
#define BNO055_SW_REV_ID_LSB					((uint8_t)0x04)

/* Page 1 Registers */

#define BNO055_GYR_AM_SET						((uint8_t)0x1F)
#define BNO055_GYR_AM_THRES						((uint8_t)0x1E)
#define BNO055_GYR_DUR_Z						((uint8_t)0x1D)
#define BNO055_GYR_HR_Z_SET						((uint8_t)0x1C)
#define BNO055_GYR_DUR_Y						((uint8_t)0x1B)
#define BNO055_GYR_HR_Y_SET						((uint8_t)0x1A)
#define BNO055_GYR_DUR_X						((uint8_t)0x19)
#define BNO055_GYR_HR_X_SET						((uint8_t)0x18)
#define BNO055_GYR_INT_SETTING					((uint8_t)0x17)
#define BNO055_ACC_NM_SET						((uint8_t)0x16)
#define BNO055_ACC_NM_THRES						((uint8_t)0x15)
#define BNO055_ACC_HG_THRES						((uint8_t)0x14)
#define BNO055_ACC_HG_DURATION					((uint8_t)0x13)
#define BNO055_ACC_INT_SETTING					((uint8_t)0x12)
#define BNO055_ACC_AM_THRES						((uint8_t)0x11)
#define BNO055_INT_EN							((uint8_t)0x10)
#define BNO055_INT_MASK							((uint8_t)0x0F)
#define BNO055_GYR_SLEEP_CONFIG					((uint8_t)0x0D)
#define BNO055_ACC_SLEEP_CONFIG					((uint8_t)0x0C)
#define BNO055_GYR_CONFIG_1						((uint8_t)0x0B)
#define BNO055_GYR_CONFIG_0						((uint8_t)0x0A)
#define BNO055_MAG_CONFIG						((uint8_t)0x09)
#define BNO055_ACC_CONFIG						((uint8_t)0x08)

/* Register Values */

#define BNO055_POWER_MODE_NORMAL				((uint8_t)0x00)
#define BNO055_POWER_MODE_LOW					((uint8_t)0x01)
#define BNO055_POWER_MODE_SUSPEND				((uint8_t)0x02)

#define BNO055_OP_MODE_CONFIG					((uint8_t)0x00)
#define BNO055_OP_MODE_ACCONLY					((uint8_t)0x01)
#define BNO055_OP_MODE_MAGONLY					((uint8_t)0x02)
#define BNO055_OP_MODE_GYRONLY					((uint8_t)0x03)
#define BNO055_OP_MODE_ACCMAG					((uint8_t)0x04)
#define BNO055_OP_MODE_ACCGYRO					((uint8_t)0x05)
#define BNO055_OP_MODE_MAGGYRO					((uint8_t)0x06)
#define BNO055_OP_MODE_AMG						((uint8_t)0x07)
#define BNO055_OP_MODE_IMU						((uint8_t)0x08)
#define BNO055_OP_MODE_COMPASS					((uint8_t)0x09)
#define BNO055_OP_MODE_M4G						((uint8_t)0x0A)
#define BNO055_OP_MODE_NDOF_FMC_OFF				((uint8_t)0x0B)
#define BNO055_OP_MODE_NDOF						((uint8_t)0x0C)

#define BNO055_REMAP_AXIS_TO_X					((uint8_t)0x00)
#define BNO055_REMAP_AXIS_TO_Y					((uint8_t)0x01)
#define BNO055_REMAP_AXIS_TO_Z					((uint8_t)0x02)

#define BNO055_REMAP_AXIS_POSITIVE				((uint8_t)0x00)
#define BNO055_REMAP_AXIS_NEGATIVE				((uint8_t)0x01)

#define BNO055_ACC_RANGE_2G						((uint8_t)0x00)
#define BNO055_ACC_RANGE_4G						((uint8_t)0x01)
#define BNO055_ACC_RANGE_8G						((uint8_t)0x02)
#define BNO055_ACC_RANGE_16G					((uint8_t)0x03)
#define BNO055_ACC_BANDWIDTH_7_81 				((uint8_t)0x00)
#define BNO055_ACC_BANDWIDTH_15_63				((uint8_t)0x01)
#define BNO055_ACC_BANDWIDTH_31_25				((uint8_t)0x02)
#define BNO055_ACC_BANDWIDTH_62_5				((uint8_t)0x03)
#define BNO055_ACC_BANDWIDTH_125				((uint8_t)0x04)
#define BNO055_ACC_BANDWIDTH_250				((uint8_t)0x05)
#define BNO055_ACC_BANDWIDTH_500				((uint8_t)0x06)
#define BNO055_ACC_BANDWIDTH_1000				((uint8_t)0x07)
#define BNO055_ACC_OP_MODE_NORMAL				((uint8_t)0x00)
#define BNO055_ACC_OP_MODE_SUSPEND				((uint8_t)0x01)
#define BNO055_ACC_OP_MODE_LOW_1				((uint8_t)0x02)
#define BNO055_ACC_OP_MODE_STANDBY				((uint8_t)0x03)
#define BNO055_ACC_OP_MODE_LOW_2				((uint8_t)0x04)
#define BNO055_ACC_OP_MODE_DEEP_SUSPEND			((uint8_t)0x05)

#define BNO055_GYR_RANGE_2000					((uint8_t)0x00)
#define BNO055_GYR_RANGE_1000					((uint8_t)0x01)
#define BNO055_GYR_RANGE_500					((uint8_t)0x02)
#define BNO055_GYR_RANGE_250					((uint8_t)0x03)
#define BNO055_GYR_RANGE_125					((uint8_t)0x04)
#define BNO055_GYR_BANDWIDTH_523				((uint8_t)0x00)
#define BNO055_GYR_BANDWIDTH_230				((uint8_t)0x01)
#define BNO055_GYR_BANDWIDTH_116				((uint8_t)0x02)
#define BNO055_GYR_BANDWIDTH_47					((uint8_t)0x03)
#define BNO055_GYR_BANDWIDTH_23					((uint8_t)0x04)
#define BNO055_GYR_BANDWIDTH_12					((uint8_t)0x05)
#define BNO055_GYR_BANDWIDTH_64					((uint8_t)0x06)
#define BNO055_GYR_BANDWIDTH_32					((uint8_t)0x07)
#define BNO055_GYR_OP_MODE_NORMAL				((uint8_t)0x00)
#define BNO055_GYR_OP_MODE_FAST_PU				((uint8_t)0x01)
#define BNO055_GYR_OP_MODE_DEEP_SUSPEND			((uint8_t)0x02)
#define BNO055_GYR_OP_MODE_SUSPEND				((uint8_t)0x03)
#define BNO055_GYR_OP_MODE_ADVANCED				((uint8_t)0x04)

#define BNO055_MAG_BANDWIDTH_2					((uint8_t)0x00)
#define BNO055_MAG_BANDWIDTH_6					((uint8_t)0x01)
#define BNO055_MAG_BANDWIDTH_8					((uint8_t)0x02)
#define BNO055_MAG_BANDWIDTH_10					((uint8_t)0x03)
#define BNO055_MAG_BANDWIDTH_15					((uint8_t)0x04)
#define BNO055_MAG_BANDWIDTH_20					((uint8_t)0x05)
#define BNO055_MAG_BANDWIDTH_25					((uint8_t)0x06)
#define BNO055_MAG_BANDWIDTH_30					((uint8_t)0x07)
#define BNO055_MAG_OP_MODE_LOW_POWER			((uint8_t)0x00)
#define BNO055_MAG_OP_MODE_REGULAR				((uint8_t)0x01)
#define BNO055_MAG_OP_MODE_ENHANCED				((uint8_t)0x02)
#define BNO055_MAG_OP_MODE_HIGH_ACCURACY		((uint8_t)0x03)
#define BNO055_MAG_POW_MODE_NORMAL				((uint8_t)0x00)
#define BNO055_MAG_POW_MODE_SLEEP				((uint8_t)0x01)
#define BNO055_MAG_POW_MODE_SUSPEND				((uint8_t)0x02)
#define BNO055_MAG_POW_MODE_FORCED				((uint8_t)0x03)

#define BNO055_UNIT_SEL_VECTOR_M_PER_S			((uint8_t)0x00)
#define BNO055_UNIT_SEL_VECTOR_MG				((uint8_t)0x01)
#define BNO055_UNIT_SEL_ANGULAR_DPS				((uint8_t)0x00)
#define BNO055_UNIT_SEL_ANGULAR_RPS				((uint8_t)0x01)
#define BNO055_UNIT_SEL_EULER_DEG				((uint8_t)0x00)
#define BNO055_UNIT_SEL_EULER_RAD				((uint8_t)0x01)
#define BNO055_UNIT_SEL_TEMP_C					((uint8_t)0x00)
#define BNO055_UNIT_SEL_TEMP_F					((uint8_t)0x01)



typedef struct{
	float pitch;
	float roll;
	float heading;
}Euler_Orientation;

typedef struct{
	int16_t w;
	int16_t x;
	int16_t y;
	int16_t z;
}Quaternion_Orientation;

typedef struct{
	int16_t x;
	int16_t y;
	int16_t z;
}Generic_XYZ;


uint8_t BNO055_Set_Page_Id(uint8_t page);

uint16_t BNO055_Read_SW_Version(void);

uint8_t BNO055_Read_BL_Version(void);

uint8_t BNO055_Read_Op_Mode(void);

uint8_t BNO055_Read_Self_Test(void);

uint8_t BNO055_Set_Euler_Mode(void);

uint8_t BNO055_Remap_Axis(uint8_t x, uint8_t y, uint8_t z);

uint8_t BNO055_Read_Axis_Map(void);

uint8_t BNO055_Remap_Axis_Sign(uint8_t sign);

uint8_t BNO055_Read_Axis_Sign(void);

uint8_t BNO055_Set_Temp_Source(uint8_t source);

uint8_t BNO055_Read_Temp_Source(void);

uint8_t BNO055_Acc_Config(uint8_t range, uint8_t bandwidth, uint8_t power_mode);

uint8_t BNO055_Read_Acc_Config(void);

uint8_t BNO055_Gyr_Config(uint8_t range, uint8_t bandwidth, uint8_t power_mode);

uint16_t BNO055_Read_Gyr_Config(void);

uint8_t BNO055_Mag_Config(uint8_t bandwidth, uint8_t operation_mode, uint8_t power_mode);

uint8_t BNO055_Read_Mag_Config(void);

uint8_t BNO055_Unit_Sel(uint8_t linear, uint8_t angular, uint8_t euler, uint8_t temperature, uint8_t format);

uint8_t BNO055_Read_Unit_Sel(void);

uint8_t BNO055_Set_Acc_Offset_X(int16_t offset);

int16_t BNO055_Read_Acc_Offset_X(void);

uint8_t BNO055_Set_Acc_Offset_Y(int16_t offset);

int16_t BNO055_Read_Acc_Offset_Y(void);

uint8_t BNO055_Set_Acc_Offset_Z(int16_t offset);

int16_t BNO055_Read_Acc_Offset_Z(void);

uint8_t BNO055_Set_Gyr_Offset_X(int16_t offset);

int16_t BNO055_Read_Gyr_Offset_X(void);

uint8_t BNO055_Set_Gyr_Offset_Y(int16_t offset);

int16_t BNO055_Read_Gyr_Offset_Y(void);

uint8_t BNO055_Set_Gyr_Offset_Z(int16_t offset);

int16_t BNO055_Read_Gyr_Offset_Z(void);

uint8_t BNO055_Set_Mag_Offset_X(int16_t offset);

int16_t BNO055_Read_Mag_Offset_X(void);

uint8_t BNO055_Set_Mag_Offset_Y(int16_t offset);

int16_t BNO055_Read_Mag_Offset_Y(void);

uint8_t BNO055_Set_Mag_Offset_Z(int16_t offset);

int16_t BNO055_Read_Mag_Offset_Z(void);

uint8_t BNO055_Set_Acc_Radius(int16_t offset);

int16_t BNO055_Read_Acc_Radius(void);

uint8_t BNO055_Set_Mag_Radius(int16_t offset);

int16_t BNO055_Read_Mag_Radius(void);

Generic_XYZ BNO055_Read_Acc_Data(void);

Generic_XYZ BNO055_Read_Mag_Data(void);

Generic_XYZ BNO055_Read_Gyr_Data(void);

Euler_Orientation BNO055_Read_Euler_Data(void);

Quaternion_Orientation BNO055_Read_Quaternion_Data(void);

Generic_XYZ BNO055_Read_Lin_Data(void);

Generic_XYZ BNO055_Read_Grv_Data(void);

int8_t BNO055_Read_Temperature(void);

int8_t BNO055_Check_Calibration(void);

int8_t BNO055_Read_Calibration(void);


#endif /* BNO055_H_ */