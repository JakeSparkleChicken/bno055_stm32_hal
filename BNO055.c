/*
 * BNO055.c
 *
 *  Created on: Jan 17, 2019
 *      Author: JakeSparkleChicken
 *     License: Public Domain
 */

#include "stm32f7xx_hal.h"
#include "BNO055.h"
#include "log.h"
#include "i2c.h"

I2C_HandleTypeDef *i2c_instance = &hi2c2;

/* The BNO055 register set larger than can be addressed with a single byte.  To get around this the registers are
 * divided into two pages.  Page 1 is mostly the threshold, sensitivity, and sleep settings for the three individual
 * MEMS devices.  Page 0 handles the rest of the functions, including data output.
 *
 * BNO055_Set_Page_Id sets the Page ID register.  All functions in this library that require registers on Page 1
 * call the function once to set it to 1, and then again to set it back to 0.
 *
 * 		INPUT: uint8_t page 	Page ID, either 0 or 1
 *
 * 		OUTPUT: 0 for error, 1 for success
 */

uint8_t BNO055_Set_Page_Id(uint8_t page)
{
	if (page > 1){
		char* msg = "Page ID Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_PAGE_ID, 1, &page, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	return 1;
}

/* BNO055_Read_SW_Version returns the software version of the Cortex M0 on the BNO055.
 *
 * 		INPUT:  NONE
 *
 * 		OUTPUT: Software version
 */

uint16_t BNO055_Read_SW_Version(void)
{
	uint8_t sw_ver_low;
	uint8_t sw_ver_high;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_SW_REV_ID_MSB, 1, &sw_ver_high, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_SW_REV_ID_LSB, 1, &sw_ver_low, 1, 10000);
	int16_t sw_ver = ((int16_t)sw_ver_high << 8) | sw_ver_low;
	return sw_ver;
}

/* BNO055_Read_BL_Version returns the boot loader version of the Cortex M0 on the BNO055.
 *
 * 		INPUT:  NONE
 *
 * 		OUTPUT: Boot loader version
 */

uint8_t BNO055_Read_BL_Version(void)
{
	uint8_t bl_ver;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_BL_REV_ID, 1, &bl_ver, 1, 10000);
	return bl_ver;
}

/* BNO055_Set_Euler_Mode sets the BNO055 to Euler mode.  The Cortex M0 on the chip takes the raw 9 axis data
 * and returns the Euler attitudes of pitch, roll, and heading.  These are retrieved with the BNO055_Read_Euler_Data
 * function.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  0 for error, 1 for success
 */

uint8_t BNO055_Set_Euler_Mode(void)
{
	uint8_t byte = 0x1C;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_OPR_MODE, 1, &byte, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	return 1;
}

/* BNO055_Read_Op_Mode reads the Operation Mode of the BNO055.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  Operation Mode
 * 						Non-Fusion
 * 					0x00	CONFIGMODE
 * 					0x01	ACCONLY
 * 					0x02	MAGONLY
 * 					0x03	GYRONLY
 * 					0x04	ACCMAG
 * 					0x05	ACCGYR
 * 					0x06	MAGGYR
 * 					0x07	AMG
 * 						Fusion
 * 					0x08	IMU
 * 					0x09	COMPASS
 * 					0x0A	M4G
 * 					0x0B	NDOF_FMC_OFF
 * 					0x0C	NDOF
 */

uint8_t BNO055_Read_Op_Mode(void)
{
	uint8_t mode;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_OPR_MODE, 1, &mode, 1, 10000);
	return mode;
}

/* BNO055_Read_Self_Test returns the result of the BNO055 self-test.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  Self Test results.  For all bits, 1 is success, 0 is failure.  0x07 is complete success.
 * 					Bit 0:  ACC
 * 					Bit 1:  MAG
 * 					Bit 2:  GYR
 * 					Bit 3:  MCU
 */

uint8_t BNO055_Read_Self_Test(void)
{
	uint8_t mode;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ST_RESULT, 1, &mode, 1, 10000);
	return mode;
}

/* BNO055_Remap_Axis remaps the three axes if your BNO055 is not in its standard orientation.
 *
 * 		INPUT:  uint8_t x, y, z		These will contain the new axis value.
 * 				0x00	X
 * 				0x01	Y
 * 				0x02	Z
 * 				To swap X and Y, the command would be BNO055_Remap_Axis(0x01, 0x00, 0x02)
 *
 * 		OUTPUT:  0 if error, 1 if success
 *
 */

uint8_t BNO055_Remap_Axis(uint8_t x, uint8_t y, uint8_t z)
{
	if((x>2)|(y>2)|(z>2)){
		char* msg = "Invalid Axis Value";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	uint8_t remap_byte = ((z<<4)|(y<<2)|x);
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_AXIS_MAP_CONFIG, 1, &remap_byte, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	return 1;
}

/* BNO055_Read_Axis_Map reads the mapping for the three axes of your BNO055.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  Returns the mapping byte.  Default is 0x24.
 * 				Bits 5-4:  Z-Axis
 * 				Bits 3-2:  Y-Axis
 * 				Bits 1-0:  X-Axis
 * 				0x00	X
 * 				0x01	Y
 * 				0x02	Z
 *
 */

uint8_t BNO055_Read_Axis_Map(void)
{
	uint8_t axis_map;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_AXIS_MAP_CONFIG, 1, &axis_map, 1, 10000);
	return axis_map;
}

/* BNO055_Remap_Axis_Sign remaps the sign of the axes if the BNO055 is not in it's standard orientation.
 *
 * 		INPUT: 	uint8_t sign
 * 				All bits are 0 for positive, 1 for negative.
 * 				Bit 2: X-axis
 * 				Bit 1: Y-Axis
 * 				Bit 0: Z-Axis
 *
 * 		OUTPUT:  0 for error, 1 for success.
 *
 */

uint8_t BNO055_Remap_Axis_Sign(uint8_t sign)
{
	if(sign > 7){
		char* msg = "Invalid Sign Value";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_AXIS_MAP_SIGN, 1, &sign, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	return 1;
}

/* BNO055_Read_Axis_Sign reads the sign of the axes if the BNO055 is not in it's standard orientation.
 *
 * 		INPUT: 	None
 *
 * 		OUTPUT:  uint8_t sign
 * 				 All bits are 0 for positive, 1 for negative.  Default 0x00.
 * 				 Bit 2: X-axis
 * 				 Bit 1: Y-Axis
 * 				 Bit 0: Z-Axis
 *
 */

uint8_t BNO055_Read_Axis_Sign(void)
{
	uint8_t axis_sign;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_AXIS_MAP_SIGN, 1, &axis_sign, 1, 10000);
	return axis_sign;
}

/* BNO055_Set_Temp_Source sets the source for the temperature readings.
 * 		INPUT:  uint8_t source
 * 				0x00	ACC
 * 				0x01	GYR
 *
 * 		OUTPUT:  0 for failure, 1 for success
 */

uint8_t BNO055_Set_Temp_Source(uint8_t source)
{
	if(source > 1){
		char* msg = "Invalid Sign Value";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_TEMP_SOURCE, 1, &source, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	return 1;
}

/* BNO055_Read_Temp_Source reads the source for the temperature readings.
 * 		INPUT:  None
 *
 * 		OUTPUT:  uint8_t source
 * 				0x00	ACC
 * 				0x01	GYR
 */

uint8_t BNO055_Read_Temp_Source(void)
{
	uint8_t source;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_TEMP_SOURCE, 1, &source, 1, 10000);
	return source;
}


/* BNO055_Acc_Config sets the configuration for the accelerometer.
 *
 * 		INPUT:  uint8_t range, bandwidth, power_mode
 * 					range
 * 				BNO055_ACC_RANGE_2G						0x00
 *				BNO055_ACC_RANGE_4G						0x01
 *				BNO055_ACC_RANGE_8G						0x02
 *				BNO055_ACC_RANGE_16G					0x03
 *					bandwidth
 *				BNO055_ACC_BANDWIDTH_7_81 				0x00	7.81 Hz
 *				BNO055_ACC_BANDWIDTH_15_63				0x01	15.63 Hz
 *				BNO055_ACC_BANDWIDTH_31_25				0x02	31.25 Hz
 *				BNO055_ACC_BANDWIDTH_62_5				0x03	62.5 Hz
 *				BNO055_ACC_BANDWIDTH_125				0x04	125 Hz
 *				BNO055_ACC_BANDWIDTH_250				0x05	250 Hz
 *				BNO055_ACC_BANDWIDTH_500				0x06	500 Hz
 *				BNO055_ACC_BANDWIDTH_1000				0x07	1000 Hz
 *					power_mode
 *				BNO055_ACC_OP_MODE_NORMAL				0x00
 *				BNO055_ACC_OP_MODE_SUSPEND				0x01
 *				BNO055_ACC_OP_MODE_LOW_1				0x02
 *				BNO055_ACC_OP_MODE_STANDBY				0x03
 *				BNO055_ACC_OP_MODE_LOW_2				0x04
 *				BNO055_ACC_OP_MODE_DEEP_SUSPEND			0x05
 *
 *		OUTPUT:  0 for failure, 1 for success
 */

uint8_t BNO055_Acc_Config(uint8_t range, uint8_t bandwidth, uint8_t power_mode)
{
	if((range>3)|(bandwidth>7)|(power_mode>5)){
		char* msg = "Invalid Acc Config";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	uint8_t config_byte = ((power_mode<<5)|(bandwidth<<2)|range);
	uint8_t page = 1;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_PAGE_ID, 1, &page, 1, 10000)){
			char* msg = "IMU Error";
			Log(msg, LOG_EVERYTHING);
			HAL_Delay(1000);
			return 0;
		}
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_CONFIG, 1, &config_byte, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	page = 0;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_PAGE_ID, 1, &page, 1, 10000)){
			char* msg = "IMU Error";
			Log(msg, LOG_EVERYTHING);
			HAL_Delay(1000);
			return 0;
	}
	return 1;
}

/* BNO055_Read_Acc_Config sets the configuration for the accelerometer.
 *
 * 		INPUT:  None
 *
 *		OUTPUT:  uint8_t config_byte
 * 					Bits 0-1:  Range
 * 				BNO055_ACC_RANGE_2G						0x00
 *				BNO055_ACC_RANGE_4G						0x01
 *				BNO055_ACC_RANGE_8G						0x02
 *				BNO055_ACC_RANGE_16G					0x03
 *
 *					Bits 4-2:  Bandwidth
 *				BNO055_ACC_BANDWIDTH_7_81 				0x00	7.81 Hz
 *				BNO055_ACC_BANDWIDTH_15_63				0x01	15.63 Hz
 *				BNO055_ACC_BANDWIDTH_31_25				0x02	31.25 Hz
 *				BNO055_ACC_BANDWIDTH_62_5				0x03	62.5 Hz
 *				BNO055_ACC_BANDWIDTH_125				0x04	125 Hz
 *				BNO055_ACC_BANDWIDTH_250				0x05	250 Hz
 *				BNO055_ACC_BANDWIDTH_500				0x06	500 Hz
 *				BNO055_ACC_BANDWIDTH_1000				0x07	1000 Hz
 *
 *					Bits 7-5:  Power Mode
 *				BNO055_ACC_OP_MODE_NORMAL				0x00
 *				BNO055_ACC_OP_MODE_SUSPEND				0x01
 *				BNO055_ACC_OP_MODE_LOW_1				0x02
 *				BNO055_ACC_OP_MODE_STANDBY				0x03
 *				BNO055_ACC_OP_MODE_LOW_2				0x04
 *				BNO055_ACC_OP_MODE_DEEP_SUSPEND			0x05
 */

uint8_t BNO055_Read_Acc_Config(void)
{
	uint8_t config_byte;
	uint8_t page = 1;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_PAGE_ID, 1, &page, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_CONFIG, 1, &config_byte, 1, 10000);
	page = 0;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_PAGE_ID, 1, &page, 1, 10000)){
			char* msg = "IMU Error";
			Log(msg, LOG_EVERYTHING);
			HAL_Delay(1000);
			return 0;
	}
	return config_byte;
}

/* BNO055_Gyr_Config sets the configuration for the gyroscope.
 *
 * 		INPUT:  uint8_t range, bandwidth, power_mode
 *
 * 					range in degrees per second
 * 				BNO055_GYR_RANGE_2000				0x00
 *				BNO055_GYR_RANGE_1000				0x01
 *				BNO055_GYR_RANGE_500				0x02
 *				BNO055_GYR_RANGE_250				0x03
 *				BNO055_GYR_RANGE_125				0x04
 *
 *					bandwidth in Hz
 *				BNO055_GYR_BANDWIDTH_523			0x00
 *				BNO055_GYR_BANDWIDTH_230			0x01
 *				BNO055_GYR_BANDWIDTH_116			0x02
 *				BNO055_GYR_BANDWIDTH_47				0x03
 *				BNO055_GYR_BANDWIDTH_23				0x04
 *				BNO055_GYR_BANDWIDTH_12				0x05
 *				BNO055_GYR_BANDWIDTH_64				0x06
 *				BNO055_GYR_BANDWIDTH_32				0x07
 *
 *					power_mode
 *				BNO055_GYR_OP_MODE_NORMAL			0x00
 *				BNO055_GYR_OP_MODE_FAST_PU			0x01
 *				BNO055_GYR_OP_MODE_DEEP_SUSPEND		0x02
 *				BNO055_GYR_OP_MODE_SUSPEND			0x03
 *				BNO055_GYR_OP_MODE_ADVANCED			0x04
 *
 *		OUTPUT:  0 for failure, 1 for success
 */

uint8_t BNO055_Gyr_Config(uint8_t range, uint8_t bandwidth, uint8_t power_mode)
{
	if((range>4)|(bandwidth>7)|(power_mode>4)){
		char* msg = "Invalid Gyr Config";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	uint8_t config_byte_1 = ((bandwidth<<3)|range);
	uint8_t page = 1;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_PAGE_ID, 1, &page, 1, 10000)){
			char* msg = "IMU Error";
			Log(msg, LOG_EVERYTHING);
			HAL_Delay(1000);
			return 0;
		}
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_CONFIG_0, 1, &config_byte_1, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_CONFIG_1, 1, &power_mode, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	page = 0;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_PAGE_ID, 1, &page, 1, 10000)){
			char* msg = "IMU Error";
			Log(msg, LOG_EVERYTHING);
			HAL_Delay(1000);
			return 0;
	}
	return 1;
}

/* BNO055_Read_Gyr_Config reads the configuration for the gyroscope.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  uint16_t config_bytes
 *
 *					Bits 13-11:  Bandwidth in Hz
 *				BNO055_GYR_BANDWIDTH_523			0x00
 *				BNO055_GYR_BANDWIDTH_230			0x01
 *				BNO055_GYR_BANDWIDTH_116			0x02
 *				BNO055_GYR_BANDWIDTH_47				0x03
 *				BNO055_GYR_BANDWIDTH_23				0x04
 *				BNO055_GYR_BANDWIDTH_12				0x05
 *				BNO055_GYR_BANDWIDTH_64				0x06
 *				BNO055_GYR_BANDWIDTH_32				0x07
 *
 * 					Bits 10-8:  Range in degrees per second
 * 				BNO055_GYR_RANGE_2000				0x00
 *				BNO055_GYR_RANGE_1000				0x01
 *				BNO055_GYR_RANGE_500				0x02
 *				BNO055_GYR_RANGE_250				0x03
 *				BNO055_GYR_RANGE_125				0x04
 *
 *					Bits 2-0:  Power Mode
 *				BNO055_GYR_OP_MODE_NORMAL			0x00
 *				BNO055_GYR_OP_MODE_FAST_PU			0x01
 *				BNO055_GYR_OP_MODE_DEEP_SUSPEND		0x02
 *				BNO055_GYR_OP_MODE_SUSPEND			0x03
 *				BNO055_GYR_OP_MODE_ADVANCED			0x04
 *
 */

uint16_t BNO055_Read_Gyr_Config(void)
{
	uint16_t config_bytes;
	uint8_t config_byte_0;
	uint8_t config_byte_1;
	uint8_t page = 1;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_PAGE_ID, 1, &page, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_CONFIG_0, 1, &config_byte_0, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_CONFIG_1, 1, &config_byte_1, 1, 10000);
	page = 0;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_PAGE_ID, 1, &page, 1, 10000)){
			char* msg = "IMU Error";
			Log(msg, LOG_EVERYTHING);
			HAL_Delay(1000);
			return 0;
	}
	config_bytes = ((uint16_t)config_byte_0<<8)|config_byte_1;
	return config_bytes;
}

/* BNO055_Mag_Config sets the configuration for the magnetometer.
 *
 * 		INPUT:  uint8_t bandwidth, operation_mode, power_mode
 * 					bandwidth in Hz
 *				BNO055_MAG_BANDWIDTH_2				0x00
 *				BNO055_MAG_BANDWIDTH_6				0x01
 *				BNO055_MAG_BANDWIDTH_8				0x02
 *				BNO055_MAG_BANDWIDTH_10				0x03
 *				BNO055_MAG_BANDWIDTH_15				0x04
 *				BNO055_MAG_BANDWIDTH_20				0x05
 *				BNO055_MAG_BANDWIDTH_25				0x06
 *				BNO055_MAG_BANDWIDTH_30				0x07
 *
 *					operation_mode
 *				BNO055_MAG_OP_MODE_LOW_POWER		0x00
 *				BNO055_MAG_OP_MODE_REGULAR			0x01
 *				BNO055_MAG_OP_MODE_ENHANCED			0x02
 *				BNO055_MAG_OP_MODE_HIGH_ACCURACY	0x03
 *
 *					power_mode
 *				BNO055_MAG_POW_MODE_NORMAL			0x00
 *				BNO055_MAG_POW_MODE_SLEEP			0x01
 *				BNO055_MAG_POW_MODE_SUSPEND			0x02
 *				BNO055_MAG_POW_MODE_FORCED			0x03
 *
 *		OUTPUT:  0 for failure, 1 for success
 */

uint8_t BNO055_Mag_Config(uint8_t bandwidth, uint8_t operation_mode, uint8_t power_mode)
{
	if((bandwidth>7)|(operation_mode>3)|(power_mode>3)){
		char* msg = "Invalid Mag Config";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	uint8_t config_byte = ((power_mode<<6)|(operation_mode<<3)|bandwidth);
	uint8_t page = 1;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_PAGE_ID, 1, &page, 1, 10000)){
			char* msg = "IMU Error";
			Log(msg, LOG_EVERYTHING);
			HAL_Delay(1000);
			return 0;
		}
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_CONFIG, 1, &config_byte, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	page = 0;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_PAGE_ID, 1, &page, 1, 10000)){
			char* msg = "IMU Error";
			Log(msg, LOG_EVERYTHING);
			HAL_Delay(1000);
			return 0;
	}
	return 1;
}

/* BNO055_Read_Mag_Config sets the configuration for the magnetometer.
 *
 * 		INPUT:  None
 *
 *		OUTPUT:  uint8_t config_byte
 *
 * 					Bits 2-0:  Bandwidth in Hz
 *				BNO055_MAG_BANDWIDTH_2				0x00
 *				BNO055_MAG_BANDWIDTH_6				0x01
 *				BNO055_MAG_BANDWIDTH_8				0x02
 *				BNO055_MAG_BANDWIDTH_10				0x03
 *				BNO055_MAG_BANDWIDTH_15				0x04
 *				BNO055_MAG_BANDWIDTH_20				0x05
 *				BNO055_MAG_BANDWIDTH_25				0x06
 *				BNO055_MAG_BANDWIDTH_30				0x07
 *
 *					Bits 4-3:  Operation Mode
 *				BNO055_MAG_OP_MODE_LOW_POWER		0x00
 *				BNO055_MAG_OP_MODE_REGULAR			0x01
 *				BNO055_MAG_OP_MODE_ENHANCED			0x02
 *				BNO055_MAG_OP_MODE_HIGH_ACCURACY	0x03
 *
 *					Bits 6-5:  Power Mode
 *				BNO055_MAG_POW_MODE_NORMAL			0x00
 *				BNO055_MAG_POW_MODE_SLEEP			0x01
 *				BNO055_MAG_POW_MODE_SUSPEND			0x02
 *				BNO055_MAG_POW_MODE_FORCED			0x03
 */

uint8_t BNO055_Read_Mag_Config(void)
{
	uint8_t config_byte;
	uint8_t page = 1;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_PAGE_ID, 1, &page, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_CONFIG, 1, &config_byte, 1, 10000);
	page = 0;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_PAGE_ID, 1, &page, 1, 10000)){
			char* msg = "IMU Error";
			Log(msg, LOG_EVERYTHING);
			HAL_Delay(1000);
			return 0;
	}
	return config_byte;
}

/* BNO055_Unit_Sel sets the units for the accelerometer, gyroscope, Euler readings, and temperature.  Also
 * sets whether fusion output will be in Windows or Android format.  Magnetometer is always in microteslas,
 * quaternions are unitless.
 *
 * 		INPUT:  uint8_t linear, angular, euler, temperature, format
 *
 * 					linear
 * 				BNO055_UNIT_SEL_VECTOR_M_PER_S		0x00	meters/sec
 * 				BNO055_UNIT_SEL_VECTOR_MG			0x01	micro-Gs
 *
 * 					angular
 * 				BNO055_UNIT_SEL_ANGULAR_DPS			0x00	Degrees/sec
 * 				BNO055_UNIT_SEL_ANGULAR_RPS			0x01	Radians/sec
 *
 * 					euler
 * 				BNO055_UNIT_SEL_EULER_DEG			0x00	Degrees
 * 				BNO055_UNIT_SEL_EULER_RAD			0x01	Radians
 *
 * 					format
 * 				BNO055_UNIT_SEL_TEMP_C				0x00	Celcius
 * 				BNO055_UNIT_SEL_TEMP_F				0x01	Fahrenheit
 *
 * 		OUTPUT:  0 for failure, 1 for success
 */

uint8_t BNO055_Unit_Sel(uint8_t linear, uint8_t angular, uint8_t euler, uint8_t temperature, uint8_t format)
{
	if((linear>1)|(angular>1)|(euler>1)|(temperature>1)|(format>1)){
		char* msg = "Invalid Unit Value";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	uint8_t unit_sel_byte = ((format<<7)|(temperature<<4)|(euler<<2)|(angular<<1)|(linear));
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_UNIT_SEL, 1, &unit_sel_byte, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	return 1;
}

/* BNO055_Read_Unit_Sel reads the units for the accelerometer, gyroscope, Euler readings, and temperature.  Also
 * reads whether fusion output will be in Windows or Android format.  Magnetometer is always in microteslas,
 * quaternions are unitless.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  uint8_t config_byte
 *
 * 					Bit 0:  Linear, Acceleration, Gravity Vector
 * 				BNO055_UNIT_SEL_VECTOR_M_PER_S		0x00	meters/sec
 * 				BNO055_UNIT_SEL_VECTOR_MG			0x01	micro-Gs
 *
 * 					Bit 1:  Angular Rate
 * 				BNO055_UNIT_SEL_ANGULAR_DPS			0x00	Degrees/sec
 * 				BNO055_UNIT_SEL_ANGULAR_RPS			0x01	Radians/sec
 *
 * 					Bit 2:  Euler Angles
 * 				BNO055_UNIT_SEL_EULER_DEG			0x00	Degrees
 * 				BNO055_UNIT_SEL_EULER_RAD			0x01	Radians
 *
 * 					Bit 3:  Temperature Format
 * 				BNO055_UNIT_SEL_TEMP_C				0x00	Celcius
 * 				BNO055_UNIT_SEL_TEMP_F				0x01	Fahrenheit
 *
 */

uint8_t BNO055_Read_Unit_Sel(void)
{
	uint8_t unit_sel;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_UNIT_SEL, 1, &unit_sel, 1, 10000);
	return unit_sel;
}

/* BNO055_Set_Acc_Offset_X sets the accelerometer X-axis offset for calibration.
 *
 * 		INPUT:  int16_t offset
 *
 * 				ACC Range 2G		+-2000
 * 				ACC Range 4G		+-4000
 * 				ACC Range 8G		+-8000
 * 				ACC Range 16G		+-16000
 *
 * 				ACC meters/sec^2	1 m/s^2 = 100 LSB
 * 				ACC micro-Gs		1 mG = 1 LSB
 *
 * 		OUTPUT:  0 for failure, 1 for success
 */

uint8_t BNO055_Set_Acc_Offset_X(int16_t offset)
{
	uint8_t msb = (offset>>8)&0xFF;
	uint8_t lsb = offset*0xFF;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_OFFSET_X_MSB, 1, &msb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_OFFSET_X_LSB, 1, &lsb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	return 1;
}

/* BNO055_Read_Acc_Offset_X reads the accelerometer X-axis offset for calibration.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  int16_t offset
 *
 * 				ACC Range 2G		+-2000
 * 				ACC Range 4G		+-4000
 * 				ACC Range 8G		+-8000
 * 				ACC Range 16G		+-16000
 *
 * 				ACC meters/sec^2	1 m/s^2 = 100 LSB
 * 				ACC micro-Gs		1 mG = 1 LSB
 *
 */

int16_t BNO055_Read_Acc_Offset_X(void)
{
	uint8_t lsb;
	uint8_t msb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_OFFSET_X_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_OFFSET_X_LSB, 1, &lsb, 1, 10000);
	int16_t offset = ((int16_t)msb << 8) | lsb;
	return offset;
}

/* BNO055_Set_Acc_Offset_Y sets the accelerometer Y-axis offset for calibration.
 *
 * 		INPUT:  int16_t offset
 *
 * 				ACC Range 2G		+-2000
 * 				ACC Range 4G		+-4000
 * 				ACC Range 8G		+-8000
 * 				ACC Range 16G		+-16000
 *
 * 				ACC meters/sec^2	1 m/s^2 = 100 LSB
 * 				ACC micro-Gs		1 mG = 1 LSB
 *
 * 		OUTPUT:  0 for failure, 1 for success
 */

uint8_t BNO055_Set_Acc_Offset_Y(int16_t offset)
{
	uint8_t msb = (offset>>8)&0xFF;
	uint8_t lsb = offset*0xFF;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_OFFSET_Y_MSB, 1, &msb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_OFFSET_Y_LSB, 1, &lsb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	return 1;
}

/* BNO055_Read_Acc_Offset_Y reads the accelerometer Y-axis offset for calibration.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  int16_t offset
 *
 * 				ACC Range 2G		+-2000
 * 				ACC Range 4G		+-4000
 * 				ACC Range 8G		+-8000
 * 				ACC Range 16G		+-16000
 *
 * 				ACC meters/sec^2	1 m/s^2 = 100 LSB
 * 				ACC micro-Gs		1 mG = 1 LSB
 *
 */

int16_t BNO055_Read_Acc_Offset_Y(void)
{
	uint8_t lsb;
	uint8_t msb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_OFFSET_Y_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_OFFSET_Y_LSB, 1, &lsb, 1, 10000);
	int16_t offset = ((int16_t)msb << 8) | lsb;
	return offset;
}

/* BNO055_Set_Acc_Offset_Z sets the accelerometer Z-axis offset for calibration.
 *
 * 		INPUT:  int16_t offset
 *
 * 				ACC Range 2G		+-2000
 * 				ACC Range 4G		+-4000
 * 				ACC Range 8G		+-8000
 * 				ACC Range 16G		+-16000
 *
 * 				ACC meters/sec^2	1 m/s^2 = 100 LSB
 * 				ACC micro-Gs		1 mG = 1 LSB
 *
 * 		OUTPUT:  0 for failure, 1 for success
 */

uint8_t BNO055_Set_Acc_Offset_Z(int16_t offset)
{
	uint8_t msb = (offset>>8)&0xFF;
	uint8_t lsb = offset*0xFF;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_OFFSET_Z_MSB, 1, &msb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_OFFSET_Z_LSB, 1, &lsb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	return 1;
}

/* BNO055_Read_Acc_Offset_Z reads the accelerometer Z-axis offset for calibration.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  int16_t offset
 *
 * 				ACC Range 2G		+-2000
 * 				ACC Range 4G		+-4000
 * 				ACC Range 8G		+-8000
 * 				ACC Range 16G		+-16000
 *
 * 				ACC meters/sec^2	1 m/s^2 = 100 LSB
 * 				ACC micro-Gs		1 mG = 1 LSB
 *
 */

int16_t BNO055_Read_Acc_Offset_Z(void)
{
	uint8_t lsb;
	uint8_t msb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_OFFSET_Z_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_OFFSET_Z_LSB, 1, &lsb, 1, 10000);
	int16_t offset = ((int16_t)msb << 8) | lsb;
	return offset;
}

/* BNO055_Set_Gyr_Offset_X sets the gyroscope X-Axis for calibration.
 *
 * 		INPUT:  int16_t offset
 *
 * 				GYR Range 2000		+-32000
 * 				GYR Range 1000		+-16000
 * 				GYR Range 500		+-8000
 * 				GYR Range 250		+-4000
 * 				GYR Range 125		+-2000
 *
 * 				GYR DPS				1 degree = 16 LSB
 * 				GYR RPS				1 radian = 900 LSB
 *
 * 		OUTPUT:  0 for failure, 1 for success
 */

uint8_t BNO055_Set_Gyr_Offset_X(int16_t offset)
{
	uint8_t msb = (offset>>8)&0xFF;
	uint8_t lsb = offset*0xFF;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_OFFSET_X_MSB, 1, &msb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_OFFSET_X_LSB, 1, &lsb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	return 1;
}

/* BNO055_Read_Gyr_Offset_X reads the gyroscope X-Axis for calibration.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  int16_t offset
 *
 * 				GYR Range 2000		+-32000
 * 				GYR Range 1000		+-16000
 * 				GYR Range 500		+-8000
 * 				GYR Range 250		+-4000
 * 				GYR Range 125		+-2000
 *
 * 				GYR DPS				1 degree = 16 LSB
 * 				GYR RPS				1 radian = 900 LSB
 *
 */

int16_t BNO055_Read_Gyr_Offset_X(void)
{
	uint8_t lsb;
	uint8_t msb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_OFFSET_X_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_OFFSET_X_LSB, 1, &lsb, 1, 10000);
	int16_t offset = ((int16_t)msb << 8) | lsb;
	return offset;
}

/* BNO055_Set_Gyr_Offset_Y sets the gyroscope Y-Axis for calibration.
 *
 * 		INPUT:  int16_t offset
 *
 * 				GYR Range 2000		+-32000
 * 				GYR Range 1000		+-16000
 * 				GYR Range 500		+-8000
 * 				GYR Range 250		+-4000
 * 				GYR Range 125		+-2000
 *
 * 				GYR DPS				1 degree = 16 LSB
 * 				GYR RPS				1 radian = 900 LSB
 *
 * 		OUTPUT:  0 for failure, 1 for success
 */

uint8_t BNO055_Set_Gyr_Offset_Y(int16_t offset)
{
	uint8_t msb = (offset>>8)&0xFF;
	uint8_t lsb = offset*0xFF;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_OFFSET_Y_MSB, 1, &msb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_OFFSET_Y_LSB, 1, &lsb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	return 1;
}

/* BNO055_Read_Gyr_Offset_Y reads the gyroscope Y-Axis for calibration.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  int16_t offset
 *
 * 				GYR Range 2000		+-32000
 * 				GYR Range 1000		+-16000
 * 				GYR Range 500		+-8000
 * 				GYR Range 250		+-4000
 * 				GYR Range 125		+-2000
 *
 * 				GYR DPS				1 degree = 16 LSB
 * 				GYR RPS				1 radian = 900 LSB
 *
 */

int16_t BNO055_Read_Gyr_Offset_Y(void)
{
	uint8_t lsb;
	uint8_t msb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_OFFSET_Y_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_OFFSET_Y_LSB, 1, &lsb, 1, 10000);
	int16_t offset = ((int16_t)msb << 8) | lsb;
	return offset;
}

/* BNO055_Set_Gyr_Offset_Z sets the gyroscope Z-Axis for calibration.
 *
 * 		INPUT:  int16_t offset
 *
 * 				GYR Range 2000		+-32000
 * 				GYR Range 1000		+-16000
 * 				GYR Range 500		+-8000
 * 				GYR Range 250		+-4000
 * 				GYR Range 125		+-2000
 *
 * 				GYR DPS				1 degree = 16 LSB
 * 				GYR RPS				1 radian = 900 LSB
 *
 * 		OUTPUT:  0 for failure, 1 for success
 */

uint8_t BNO055_Set_Gyr_Offset_Z(int16_t offset)
{
	uint8_t msb = (offset>>8)&0xFF;
	uint8_t lsb = offset*0xFF;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_OFFSET_Z_MSB, 1, &msb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_OFFSET_Z_LSB, 1, &lsb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	return 1;
}

/* BNO055_Read_Gyr_Offset_Z reads the gyroscope Z-Axis for calibration.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  int16_t offset
 *
 * 				GYR Range 2000		+-32000
 * 				GYR Range 1000		+-16000
 * 				GYR Range 500		+-8000
 * 				GYR Range 250		+-4000
 * 				GYR Range 125		+-2000
 *
 * 				GYR DPS				1 degree = 16 LSB
 * 				GYR RPS				1 radian = 900 LSB
 *
 */

int16_t BNO055_Read_Gyr_Offset_Z(void)
{
	uint8_t lsb;
	uint8_t msb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_OFFSET_Z_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_OFFSET_Z_LSB, 1, &lsb, 1, 10000);
	int16_t offset = ((int16_t)msb << 8) | lsb;
	return offset;
}

/* BNO055_Set_Mag_Offset_X sets the magnetometer X-axis offset for calibration.
 *
 * 		INPUT:  int16_t offset
 *
 * 				1 microtesla = 16 LSB
 *
 * 		OUTPUT:  0 for failure, 1 for success
 */

uint8_t BNO055_Set_Mag_Offset_X(int16_t offset)
{
	uint8_t msb = (offset>>8)&0xFF;
	uint8_t lsb = offset*0xFF;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_OFFSET_X_MSB, 1, &msb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_OFFSET_X_LSB, 1, &lsb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	return 1;
}

/* BNO055_Read_Mag_Offset_X reads the magnetometer X-axis offset for calibration.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  int16_t offset
 *
 * 				1 microtesla = 16 LSB
 *
 */

int16_t BNO055_Read_Mag_Offset_X(void)
{
	uint8_t lsb;
	uint8_t msb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_OFFSET_X_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_OFFSET_X_LSB, 1, &lsb, 1, 10000);
	int16_t offset = ((int16_t)msb << 8) | lsb;
	return offset;
}

/* BNO055_Set_Mag_Offset_Y sets the magnetometer Y-axis offset for calibration.
 *
 * 		INPUT:  int16_t offset
 *
 * 				1 microtesla = 16 LSB
 *
 * 		OUTPUT:  0 for failure, 1 for success
 */

uint8_t BNO055_Set_Mag_Offset_Y(int16_t offset)
{
	uint8_t msb = (offset>>8)&0xFF;
	uint8_t lsb = offset*0xFF;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_OFFSET_Y_MSB, 1, &msb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_OFFSET_Y_LSB, 1, &lsb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	return 1;
}

/* BNO055_Read_Mag_Offset_Y reads the magnetometer Y-axis offset for calibration.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  int16_t offset
 *
 * 				1 microtesla = 16 LSB
 *
 */

int16_t BNO055_Read_Mag_Offset_Y(void)
{
	uint8_t lsb;
	uint8_t msb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_OFFSET_Y_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_OFFSET_Y_LSB, 1, &lsb, 1, 10000);
	int16_t offset = ((int16_t)msb << 8) | lsb;
	return offset;
}

/* BNO055_Set_Mag_Offset_Z sets the magnetometer Z-axis offset for calibration.
 *
 * 		INPUT:  int16_t offset
 *
 * 				1 microtesla = 16 LSB
 *
 * 		OUTPUT:  0 for failure, 1 for success
 */

uint8_t BNO055_Set_Mag_Offset_Z(int16_t offset)
{
	uint8_t msb = (offset>>8)&0xFF;
	uint8_t lsb = offset*0xFF;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_OFFSET_Z_MSB, 1, &msb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_OFFSET_Z_LSB, 1, &lsb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	return 1;
}

/* BNO055_Read_Mag_Offset_Z reads the magnetometer Z-axis offset for calibration.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  int16_t offset
 *
 * 				1 microtesla = 16 LSB
 *
 */

int16_t BNO055_Read_Mag_Offset_Z(void)
{
	uint8_t lsb;
	uint8_t msb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_OFFSET_Z_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_OFFSET_Z_LSB, 1, &lsb, 1, 10000);
	int16_t offset = ((int16_t)msb << 8) | lsb;
	return offset;
}

/* BNO055_Set_Acc_Radius sets the accelerometer radius offset for calibration.
 *
 * 		INPUT:  int16_t offset
 *
 * 				Range	+-1000 LSB
 *
 * 		OUTPUT:  0 for failure, 1 for success
 */

uint8_t BNO055_Set_Acc_Radius(int16_t offset)
{
	uint8_t msb = (offset>>8)&0xFF;
	uint8_t lsb = offset*0xFF;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_RADIUS_MSB, 1, &msb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_RADIUS_LSB, 1, &lsb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	return 1;
}

/* BNO055_Read_Acc_Radius sets the accelerometer radius offset for calibration.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  int16_t offset
 *
 * 				Range	+-1000 LSB
 *
 */

int16_t BNO055_Read_Acc_Radius(void)
{
	uint8_t lsb;
	uint8_t msb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_RADIUS_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_RADIUS_LSB, 1, &lsb, 1, 10000);
	int16_t radius = ((int16_t)msb << 8) | lsb;
	return radius;
}

/* BNO055_Set_Mag_Radius sets the magnetometer radius offset for calibration.
 *
 * 		INPUT:  int16_t offset
 *
 * 				Range	+-960 LSB
 *
 * 		OUTPUT:  0 for failure, 1 for success
 */

uint8_t BNO055_Set_Mag_Radius(int16_t offset)
{
	uint8_t msb = (offset>>8)&0xFF;
	uint8_t lsb = offset*0xFF;
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_RADIUS_MSB, 1, &msb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	if(HAL_I2C_Mem_Write(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_RADIUS_LSB, 1, &lsb, 1, 10000)){
		char* msg = "IMU Error";
		Log(msg, LOG_EVERYTHING);
		HAL_Delay(1000);
		return 0;
	}
	return 1;
}

/* BNO055_Read_Mag_Radius sets the magnetometer radius offset for calibration.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  int16_t offset
 *
 * 				Range	+-960 LSB
 *
 */

int16_t BNO055_Read_Mag_Radius(void)
{
	uint8_t lsb;
	uint8_t msb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_RADIUS_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_RADIUS_LSB, 1, &lsb, 1, 10000);
	int16_t radius = ((int16_t)msb << 8) | lsb;
	return radius;
}

/* BNO055_Read_Acc_Data reads the raw accelerometer data.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  Generic_XYZ data
 * 					int16_t x
 * 					int16_t y
 * 					int16_t z
 */

Generic_XYZ BNO055_Read_Acc_Data(void)
{
	uint8_t msb;
	uint8_t lsb;
	Generic_XYZ data;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_DATA_X_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_DATA_X_LSB, 1, &lsb, 1, 10000);
	data.x = ((int16_t)msb << 8) | lsb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_DATA_Y_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_DATA_Y_LSB, 1, &lsb, 1, 10000);
	data.y = ((int16_t)msb << 8) | lsb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_DATA_Z_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_ACC_DATA_Z_LSB, 1, &lsb, 1, 10000);
	data.z = ((int16_t)msb << 8) | lsb;
	return data;

}

/* BNO055_Read_Mag_Data reads the raw magnetometer data.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  Generic_XYZ data
 * 					int16_t x
 * 					int16_t y
 * 					int16_t z
 */

Generic_XYZ BNO055_Read_Mag_Data(void)
{
	uint8_t msb;
	uint8_t lsb;
	Generic_XYZ data;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_DATA_X_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_DATA_X_LSB, 1, &lsb, 1, 10000);
	data.x = ((int16_t)msb << 8) | lsb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_DATA_Y_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_DATA_Y_LSB, 1, &lsb, 1, 10000);
	data.y = ((int16_t)msb << 8) | lsb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_DATA_Z_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_MAG_DATA_Z_LSB, 1, &lsb, 1, 10000);
	data.z = ((int16_t)msb << 8) | lsb;
	return data;

}

/* BNO055_Read_Gyr_Data reads the raw gyroscope data.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  Generic_XYZ data
 * 					int16_t x
 * 					int16_t y
 * 					int16_t z
 */

Generic_XYZ BNO055_Read_Gyr_Data(void)
{
	uint8_t msb;
	uint8_t lsb;
	Generic_XYZ data;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_DATA_X_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_DATA_X_LSB, 1, &lsb, 1, 10000);
	data.x = ((int16_t)msb << 8) | lsb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_DATA_Y_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_DATA_Y_LSB, 1, &lsb, 1, 10000);
	data.y = ((int16_t)msb << 8) | lsb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_DATA_Z_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GYR_DATA_Z_LSB, 1, &lsb, 1, 10000);
	data.z = ((int16_t)msb << 8) | lsb;
	return data;

}

/* BNO055_Read_Euler_Data reads the Euler data.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  Euler_Orientation data
 * 					int16_t pitch
 * 					int16_t roll
 * 					int16_t heading
 *
 */

Euler_Orientation BNO055_Read_Euler_Data(void)
{
	uint8_t msb;
	uint8_t lsb;
	Euler_Orientation data;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_EULER_PITCH_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_EULER_PITCH_LSB, 1, &lsb, 1, 10000);
	data.pitch = (float)((((int16_t)msb) << 8) | lsb)/16;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_EULER_ROLL_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_EULER_ROLL_LSB, 1, &lsb, 1, 10000);
	data.roll = (float)((((int16_t)msb) << 8) | lsb)/16;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_EULER_HEADING_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_EULER_HEADING_LSB, 1, &lsb, 1, 10000);
	data.heading = (float)((((int16_t)msb) << 8) | lsb)/16;
	return data;

}

/* BNO055_Read_Quaternion_Data reads the quaternion data.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  Quaternion_Orientation data
 * 					int16_t w
 * 					int16_t x
 * 					int16_t y
 * 					int16_t z
 */

Quaternion_Orientation BNO055_Read_Quaternion_Data(void)
{
	uint8_t msb;
	uint8_t lsb;
	Quaternion_Orientation data;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_QUA_DATA_W_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_QUA_DATA_W_LSB, 1, &lsb, 1, 10000);
	data.w = ((int16_t)msb << 8) | lsb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_QUA_DATA_X_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_QUA_DATA_X_LSB, 1, &lsb, 1, 10000);
	data.x = ((int16_t)msb << 8) | lsb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_QUA_DATA_Y_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_QUA_DATA_Y_LSB, 1, &lsb, 1, 10000);
	data.y = ((int16_t)msb << 8) | lsb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_QUA_DATA_Z_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_QUA_DATA_Z_LSB, 1, &lsb, 1, 10000);
	data.z = ((int16_t)msb << 8) | lsb;
	return data;
}

/* BNO055_Read_Lin_Data reads the linear acceleration data.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  Generic_XYZ data
 * 					int16_t x
 * 					int16_t y
 * 					int16_t z
 */

Generic_XYZ BNO055_Read_Lin_Data(void)
{
	uint8_t msb;
	uint8_t lsb;
	Generic_XYZ data;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_LIA_DATA_X_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_LIA_DATA_X_LSB, 1, &lsb, 1, 10000);
	data.x = ((int16_t)msb << 8) | lsb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_LIA_DATA_Y_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_LIA_DATA_Y_LSB, 1, &lsb, 1, 10000);
	data.y = ((int16_t)msb << 8) | lsb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_LIA_DATA_Z_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_LIA_DATA_Z_LSB, 1, &lsb, 1, 10000);
	data.z = ((int16_t)msb << 8) | lsb;
	return data;

}

/* BNO055_Read_Grv_Data reads the gravity vector data.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  Generic_XYZ data
 * 					int16_t x
 * 					int16_t y
 * 					int16_t z
 */

Generic_XYZ BNO055_Read_Grv_Data(void)
{
	uint8_t msb;
	uint8_t lsb;
	Generic_XYZ data;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GRV_DATA_X_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GRV_DATA_X_LSB, 1, &lsb, 1, 10000);
	data.x = ((int16_t)msb << 8) | lsb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GRV_DATA_Y_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GRV_DATA_Y_LSB, 1, &lsb, 1, 10000);
	data.y = ((int16_t)msb << 8) | lsb;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GRV_DATA_Z_MSB, 1, &msb, 1, 10000);
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_GRV_DATA_Z_LSB, 1, &lsb, 1, 10000);
	data.z = ((int16_t)msb << 8) | lsb;
	return data;

}

/* BNO055_Read_Temperature reads the temperature data.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  int8_t temperature
 */

int8_t BNO055_Read_Temperature(void)
{
	uint8_t temperature;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_TEMP, 1, &temperature, 1, 10000);
	return (int8_t)temperature;
}

/* BNO055_Read_Calibration reads the calibration status register.
 *
 * 		INPUT:  None
 *
 * 		OUTPUT:  uint8_t calibration
 * 					For all statuses, 0x03 is fully calibrated, 0x00 is uncalibrated
 * 					Bits 7-6:  System calibration
 * 					Bits 5-4:  GYR calibration
 * 					Bits 3-2:  ACC calibration
 * 					BITS 1-0:  MAG calibration
 *
 */

int8_t BNO055_Read_Calibration(void)
{
	uint8_t calibration;
	HAL_I2C_Mem_Read(i2c_instance, BNO055_I2C_ADDRESS, BNO055_CALIB_STAT, 1, &calibration, 1, 10000);
	return calibration;
}
