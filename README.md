# BNO055_STM32_HAL

BNO055 10-Axis IMU library for STM32.  All registers and config bytes are in the
header file, but currently missing interrupt functions.

Note:  Change I2C bus to the one that you will actually be using on line 14 of 
       BNO055.c.  All of the I2C writes call a custom, generic logging library.
       If you do not have a logging method of your own, extract the function
       calls from the if statements and comment out the conditional statements.